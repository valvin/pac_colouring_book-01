Pepper & Carrot: The Inktober 2017 Colouring Book
=================================================

![Printed result](result.jpeg)

I have selected 20 most colouring-book-friendly artworks of [David Revoy](https://www.peppercarrot.com/en/static6/sources&page=inks) (inspired by [his own book](https://www.peppercarrot.com/en/article428/my-full-inktober-in-a-pdf)) and after little adjustments (cleaning/lines emphasis) I organised them into an A5 colouring book that is also captured on the photo above.

This repository contains sources and scripts for you to make your own one.

Prerequisites
-------------
* Scribus
* LaTeX
* mupdf (because of the `mutool` utility)
* pdfjam (because of the `pdfbook` utility)
* font [Drukaatie Burti](https://framagit.org/peppercarrot/webcomics/blob/master/fonts/Latin/DrukaatieBurti.ttf)
* wget (for automatic download of artworks)

Usage
-----
Just run the `build.sh` script (after properly inspecting it). It will download all missing artworks (the ones that I didn’t modify in any way), run Scribus to produce the title page and LaTeX to produce the rest of the pages. Mutool then merges both PDFs, while pdfbook prepares A4 pages in such a way that you can then print the result in a "no tumble" duplex mode and it will nicely fold into a colouring book.

Due to the print (on both inkjet and laser printers) showing through the standard 80g/m² paper, I make the prints only one-sided.

If you have paper thick enough, run `build-double_sided.sh` instead.

Licence
-------
* [All the artworks](https://www.peppercarrot.com/en/static6/sources&page=inks) were done by [David Revoy](https://www.davidrevoy.com/) and are licenced under Creative Commons Attribution 4.0.
* Ideas on automatic conversion of Scribus document into a PDF taken from [Valvin’s P&C minibook](https://framagit.org/valvin/peppercarrot_minibook).
* Everything I did is licenced under CC0.

mkdir -p tmp/
cd tmp/

baseurl="https://www.peppercarrot.com/0_sources/0ther/inks/"
download=""

for artwork in \
      2017-10-03_poison_00.jpg \
      2017-10-05_long_04.jpg \
      2017-10-07_shy_05.jpg \
      2017-10-14_fierce_00.jpg \
      2017-10-18_filthy_04.jpg \
      2017-10-21_furious_04.jpg \
      2017-10-22_trail_04.jpg \
      2017-10-27_climb_00.jpg \
    ; do
  [ ! -e $artwork ] && download="$download $baseurl$artwork"
done

if [ -n "$download" ]; then
  wget $download
fi
